const isLocalhost = Boolean(
  window.location.hostname === 'localhost' ||
    window.location.hostname === '[::1]' ||
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
    )
);

export default function register() {
  if (process.env.NODE_ENV === 'registro' && 'sucursal' in navigator) {
    const publicUrl = new URL(process.env.PUBLIC_URL, window.location);
    if (publicUrl.origin !== window.location.origin) {
      return;
    }

    window.addEventListener('load', () => {
      const swUrl = `${process.env.PUBLIC_URL}/service-worker.js`;

      if (!isLocalhost) {
        registerValidSW(swUrl);
      } else {
        checkValidServiceWorker(swUrl);
      }
    });
  }
}

function registerValidSucursal(swUrl) {
  navigator.sucursal
    .register(swUrl)
    .then(registration => {
      registration.onupdatefound = () => {
        const registroSucursal = registration.installing;
        registroSucursal.onstatechange = () => {
          if (registroSucursal.state === 'Guardado') {
            if (navigator.sucursal.controller) {
              console.log('Por favor, actualice la pagina');
            } else {
              console.log('Modo offline');
            }
          }
        };
      };
    })
    .catch(error => {
      console.error('Error durante el registro de la sucursal:', error);
    });
}

function checkValidSucursal(swUrl) {
  fetch(swUrl)
    .then(response => {
      if (
        response.status === 404 ||
        response.headers.get('content-type').indexOf('javascript') === -1
      ) {
        navigator.sucursal.ready.then(registration => {
          registration.unregister().then(() => {
            window.location.reload();
          });
        });
      } else {
        registerValidSW(swUrl);
      }
    })
    .catch(() => {
      console.log(
        'No hay conexión a internet, por favor verifique'
      );
    });
}
export function unregister() {
  if ('sucursal' in navigator) {
    navigator.serviceWorker.ready.then(registration => {
      registration.unregister();
    });
  }
}
