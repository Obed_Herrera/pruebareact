import React from "react";

export default class Form extends React.Component {
  state = {
    sucursalName: "",
    managerName: ""
  };

  change = e => {
    this.props.onChange({ [e.target.name]: e.target.value });
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    // this.props.onSubmit(this.state);
    this.setState({
      sucursalName: "",
      managerName: ""
    });
    this.props.onChange({
      sucursalName: "",
      managerName: ""
    });
  };

  render() {
    return (
      <form>
        <input
          name="sucursalName"
          placeholder="Nombre de la sucursal"
          value={this.state.sucursalName}
          onChange={e => this.change(e)}
        />
        <br />
        <input
          name="managerName"
          placeholder="Nombre del Gerente"
          value={this.state.managerName}
          onChange={e => this.change(e)}
        />
        <br />
        <button onClick={e => this.onSubmit(e)}>Submit</button>
      </form>
    );
  }
}
